const gulp = require('gulp');
const zip = require('gulp-zip');
var fileindex = require('gulp-fileindex');

gulp.task('zipperesb', ()=>
    gulp.src('ESB/src/*')
        .pipe(zip('esb.zip'))
        .pipe(gulp.dest('dist'))
);
gulp.task('zipperpiloto', ()=>
    gulp.src('Piloto/src/*')
        .pipe(zip('piloto.zip'))
        .pipe(gulp.dest('dist'))
);
gulp.task('zipperrastreo', ()=>
    gulp.src('Rastreo/src/*')
        .pipe(zip('rastreo.zip'))
        .pipe(gulp.dest('dist'))
);
gulp.task('zippersolicitud', ()=>
    gulp.src('Solicitud/src/*')
        .pipe(zip('solicitud.zip'))
        .pipe(gulp.dest('dist'))
);
gulp.task('zippercliente', ()=>
    gulp.src('Cliente/src/*')
        .pipe(zip('cliente.zip'))
        .pipe(gulp.dest('dist'))
);
gulp.task('fileindex', function() {
    return gulp.src('dist/*.zip')
        .pipe(fileindex())
        .pipe(gulp.dest('./dist'));
});

gulp.task('default', gulp.series('zipperesb','zipperpiloto','zipperrastreo',
                                'zippersolicitud','zippercliente','fileindex'));
