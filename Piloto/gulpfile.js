const gulp = require('gulp');
const zip = require('gulp-zip');

gulp.task('default', ()=>
    gulp.src('src/*')
        .pipe(zip('piloto.zip'))
        .pipe(gulp.dest('dist'))
);