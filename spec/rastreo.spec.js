const Request = require("request");

describe("Servicio Rastreo", ()=>{
    var server;
    beforeAll( ()=> {
        server = require("../Rastreo/src/ubicarAuto");
    });
    afterAll(() =>{
        server.close();
    });
    describe("GET /ubicarAuto", ()=>{
        var data = {};
        beforeAll((done)=> {
            Request.get("http://localhost:303/ubicarAuto/?id=1", (error, response, body) => {
                data.status = response.statusCode;    
                data.body = JSON.parse(body);
                done();
            });
        });
        it("Estado 200", () =>{
            expect(data.status).toBe(200);
        });
        it("Body JSON", () =>{
            expect(data.body.salida).toEqual({placaAuto:'523123',idPiloto:1,direccion: "6 av 7 calle",modelo:"Toyota"});
        });
    });
});