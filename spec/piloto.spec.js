const Request = require("request");

describe("Servicio Piloto", ()=>{
    var server;
    beforeAll( ()=> {
        server = require("../Piloto/src/avisoPiloto");
    });
    afterAll(() =>{
        server.close();
    });
    describe("GET /avisarPiloto", ()=>{
        var data = {};
        beforeAll((done)=> {
            Request.get("http://localhost:302/avisarPiloto/?ubicacion=Mixco", (error, response, body) => {
                data.status = response.statusCode;    
                data.body = JSON.parse(body);
                done();
            });
        });
        it("Estado 200", () =>{
            expect(data.status).toBe(200);
        });
        it("Body JSON", () =>{
            expect(data.body.salida).toEqual({ idAuto:3,idPiloto:2,nombrePiloto:'Juan'});
        });
    });
});