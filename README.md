# Software Avanzado
## Tarea 5

## Diagrama de la aplicacion
![diagrama app](diagrama1.png)

## Ejemplos de peticiones

### Solicitud
`http://localhost:301/solicitarViaje?ubicacion=mixco`

#### Piloto
`http://localhost:302/avisarPiloto?ubicacion=Mixco`

#### Rastreo
`http://localhost:303/ubicarAuto?id=1`


## Pruebas Unitarias

### Instalar Jasmin
Instalar dependencia
```yml
npm install jasmine --save-dev
```
Agregar Scripts a `package.json`
```json
"test-init": "node ./node_modules/jasmine/bin/jasmine.js init",
"test": "node ./node_modules/jasmine/bin/jasmine.js"
```
#### Correr Jasmin
Crear el primer spec
```yml
npm run test-init
```
Ir a la carpeta `spec/support` y crear el archivo `app.spec.js` con todas las pruebas. Luego probarlas con:
```yml
npm run test
```
#### Instalar Request
```yml
npm install request
```

### Instalar ESLINT
Mas info en <https://eslint.org/docs/user-guide/getting-started>
Instalar dependencia
```yml
npm install --save-dev eslint eslint-config-strongloop
```
- Hay que ignorar lo que este en `node_modules` para ello: `echo node_modules/ >> .gitignore`
### Crear archivo eslintrc
```yml
node ./node_modules/eslint/bin/eslint --init
```
### Ejecutar ESLINT
Con el comando:
```yml
node ./node_modules/eslint/bin/eslint --ignore-path .gitignore .
```
o con el script del package.json:
```yml
npm run pretest
```
#### Agregar Scripts a `package.json`
```json
"pretest": "node ./node_modules/eslint/bin/eslint --ignore-path .gitignore ."
```
#### Agregar a los servicios
```typescript
module.exports = server;
```

#### Agregar todo lo que queramos que nos ignore a .env
```json
    "env": {
        "browser": true, // el console
        "commonjs": true,
        "es6": true,
        "node": true,  // el const
        "test": true,
        "jasmine":true // las pruebas
    }
```
#### Hacer manual el script
- Hacer que `eslint` use la configuracion recomendada, en el archivo `.eslintrc.json`, agregar en el las siguientes lineas
```json
{
    "env": {
        "browser": true,
        "commonjs": true,
        "es6": true,
        "node": true,
        "test": true,
        "jasmine":true
    },
    "extends": [
        "eslint:recommended"
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaVersion": 2018
    },
    "rules": {
        "semi": ["error", "always"]
    }
}
```